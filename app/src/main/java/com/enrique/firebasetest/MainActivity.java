package com.enrique.firebasetest;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

	private List<User> userList = new ArrayList<>();

	private RecyclerView recyclerView;
	private MyAdapter mAdapter;

	private FirebaseAuth mAuth;
	private FirebaseAuth.AuthStateListener mAuthListener;

	Button button;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAuth =  FirebaseAuth.getInstance();
		prepareRegistrationForm();
	//	createTopic();
//		addImage();
//		downloadImage();
	}



	private void addValue() {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference myRef = database.getReference("message");
		myRef.setValue("Hello, World!");
	}


	private void addValueWithId() {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		database.getReference().push().setValue("Hu");
	}

	private void mirroring() {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference myRef = database.getReference("message");
		myRef.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				System.out.println();
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
				System.out.println();
			}
		});
	}

	private void mapping() {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference myRef = database.getReference("");
		myRef.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot dataSnapshot) {
				Map<String, String> map = (Map)dataSnapshot.getValue();
				String name = map.get("Name");
				String profession =  map.get("Profession");
				String age =  map.get("Age");

				Log.v("Name ", name);
				Log.v("Profession ", profession);
				Log.v("Age ", age);
			}

			@Override
			public void onCancelled(DatabaseError databaseError) {
				System.out.println();
			}
		});
	}

	private void prepareData() {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference myRef = database.getReference("Users");
		myRef.addChildEventListener(new ChildEventListener() {
			@Override
			public void onChildAdded(DataSnapshot dataSnapshot, String s) {

				User user = new User();
				user.setName(dataSnapshot.getValue(String.class));
				userList.add(user);
				mAdapter = new MyAdapter(userList);
				recyclerView.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();
			}

			@Override
			public void onChildChanged(DataSnapshot dataSnapshot, String s) {
				userList.clear();
				User user = new User();
				user.setName(dataSnapshot.getValue(String.class));
				userList.add(user);

				mAdapter = new MyAdapter(userList);
				recyclerView.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();
			}

			@Override
			public void onChildRemoved(DataSnapshot dataSnapshot) {
				User user = new User();
				user.setName(dataSnapshot.getValue(String.class));
				userList.add(user);
				mAdapter = new MyAdapter(userList);
				recyclerView.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();
			}

			@Override
			public void onChildMoved(DataSnapshot dataSnapshot, String s) {

			}

			@Override
			public void onCancelled(DatabaseError databaseError) {

			}
		});
	}

	private void prepareListView() {
		setContentView(R.layout.list);
		recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

		mAdapter = new MyAdapter(userList);
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
		recyclerView.setLayoutManager(mLayoutManager);
		recyclerView.setItemAnimator(new DefaultItemAnimator());
		recyclerView.setAdapter(mAdapter);
	}

	private void prepareAuthForm() {
		setContentView(R.layout.login);

		mAuthListener = new FirebaseAuth.AuthStateListener() {
			@Override
			public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
				FirebaseUser user = firebaseAuth.getCurrentUser();
				if (user != null) {
					// User is signed in
					Log.d("", "onAuthStateChanged:signed_in:" + user.getUid());
				} else {
					// User is signed out
					Log.d("", "onAuthStateChanged:signed_out");
				}
				// ...
			}
		};

		button = (Button) findViewById(R.id.button3);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mAuth.signInWithEmailAndPassword("eenriquelopez@gmail.com", "123456")
						.addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {

							@Override
							public void onComplete(@NonNull Task<AuthResult> task) {
								Log.d("", "signInWithEmail:onComplete:" + task.isSuccessful());
								task.isComplete();
								// If sign in fails, display a message to the user. If sign in succeeds
								// the auth state listener will be notified and logic to handle the
								// signed in user can be handled in the listener.
								if (!task.isSuccessful()) {
									Log.w("", "signInWithEmail:failed", task.getException());
									Toast.makeText(MainActivity.this, "Failed",
											Toast.LENGTH_SHORT).show();
								}

								// ...
							}

						});
			}
		});

	}

	private void prepareRegistrationForm() {
		setContentView(R.layout.login);

		mAuthListener = new FirebaseAuth.AuthStateListener() {
			@Override
			public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
				FirebaseUser user = firebaseAuth.getCurrentUser();
				if (user != null) {
					// User is signed in
					Log.d("", "onAuthStateChanged:signed_in:" + user.getUid());
				} else {
					// User is signed out
					Log.d("", "onAuthStateChanged:signed_out");
				}
				// ...
			}
		};

		button = (Button) findViewById(R.id.button3);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				mAuth.signInWithEmailAndPassword("eenriquelopez@gmail.com", "123456")
						.addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {


							@Override
							public void onComplete(@NonNull Task<AuthResult> task) {
								if (!task.isSuccessful()) {
									Toast.makeText(MainActivity.this, "Failure",
											Toast.LENGTH_SHORT).show();


								} else {
									FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

									user.sendEmailVerification()
											.addOnCompleteListener(new OnCompleteListener<Void>() {
												@Override
												public void onComplete(@NonNull Task<Void> task) {
													if (task.isSuccessful()) {
														Log.d("", "Email sent.");
													}
												}
											});
								}
								// ...
							}

						});
			}
		});

	}

	@Override
	public void onStart() {
		super.onStart();
		mAuth.addAuthStateListener(mAuthListener);
	}

	@Override
	public void onStop() {
		super.onStop();
		if (mAuthListener != null) {
			mAuth.removeAuthStateListener(mAuthListener);
		}
	}

	private void accessUserInformation() {
		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		if (user != null) {
			// Name, email address, and profile photo Url
			String name = user.getDisplayName();
			String email = user.getEmail();
			Uri photoUrl = user.getPhotoUrl();

			// The user's ID, unique to the Firebase project. Do NOT use this value to
			// authenticate with your backend server, if you have one. Use
			// FirebaseUser.getToken() instead.
			String uid = user.getUid();
		}
	}

	private void createTopic() {
		FirebaseMessaging.getInstance().subscribeToTopic("TopicName");
	}

	private void addImage() {
		setContentView(R.layout.login);



		button = (Button) findViewById(R.id.button3);

		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Intent.ACTION_PICK);
				intent.setType("image/*");
				startActivityForResult(intent, 2);
			}
		});
	}

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, 1);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode,resultCode,data);
		if (requestCode == 2 && resultCode == RESULT_OK) {
			StorageReference storageReference = FirebaseStorage.getInstance().getReference();
			Uri uri = data.getData();
			StorageReference filepath = storageReference.child("Photos").child(uri.getLastPathSegment());
			filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
				@Override
				public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
					Log.v("T","a");
				}
			});
		}
	}

	private void downloadImage() {
		// Create a storage reference from our app
		StorageReference storageReference = FirebaseStorage.getInstance().getReference();

// Create a reference with an initial file path and name
		storageReference.child("Photos/1386939405").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
			@Override
			public void onSuccess(Uri uri) {
				// Got the download URL for 'users/me/profile.png'
				Log.v("",uri.toString());
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception exception) {
				// Handle any errors
			}
		});
	}

}
