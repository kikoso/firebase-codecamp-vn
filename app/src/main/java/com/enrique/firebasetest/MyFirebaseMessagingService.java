package com.enrique.firebasetest;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by enriquelopezmanas on 07/03/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

	private static final String TAG = "Firebase_MSG";

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		// TODO(developer): Handle FCM messages here.
		// If the application is in the foreground handle both data and notification messages here.
		// Also if you intend on generating your own notifications as a result of a received FCM
		// message, here is where that should be initiated. See sendNotification method below.
	//	sendNotification(remoteMessage.getNotification().getBody());
		Log.d(TAG, "From: " + remoteMessage.getFrom());
		Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
	}

	private void sendNotification(String messageBody) {
		Log.d(TAG, "From: " + messageBody);
	}
}