package com.enrique.firebasetest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by enriquelopezmanas on 07/03/2017.
 */


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

	private List<User> userList;

	public class MyViewHolder extends RecyclerView.ViewHolder {
		public TextView name, age, profession;

		public MyViewHolder(View view) {
			super(view);
			name = (TextView) view.findViewById(R.id.name);
			profession = (TextView) view.findViewById(R.id.age);
			age = (TextView) view.findViewById(R.id.profession);
		}
	}
	public void swap(ArrayList<User> datas){
		userList.clear();
		userList.addAll(datas);
		notifyDataSetChanged();
	}

	public MyAdapter(List<User> userList) {
		this.userList = userList;
	}

	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.list_row, parent, false);

		return new MyViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(MyViewHolder holder, int position) {
		User movie = userList.get(position);
		holder.name.setText(movie.getName());
		holder.profession.setText(movie.getProfession());
		holder.age.setText(movie.getAge());
	}

	@Override
	public int getItemCount() {
		return userList.size();
	}
}