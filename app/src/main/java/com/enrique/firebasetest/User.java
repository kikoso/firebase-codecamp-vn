package com.enrique.firebasetest;

/**
 * Created by enriquelopezmanas on 07/03/2017.
 */

public class User {
	private String age, name, profession;

	public User(String age, String name, String profession) {
		this.age = age;
		this.name = name;
		this.profession = profession;
	}

	public User() {

	}

	public String getAge() {

		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}
}
